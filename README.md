# CYBEReSCAPE
Niveau : 2de SNT
Dans cet escape game, j’ai souhaité mettre les élèves en immersion afin de leur faire découvrir quelques unes des notions de l’année en SNT : Web, Internet, réseaux sociaux, données structurées, différents types de codage et langages informatiques, avec la cybersécurité en fil rouge. Le débriefing qui suit permet de faire redescendre la pression et de leur faire dégager ces différents contenus.

## Le scénario :
Les élèves de Seconde viennent d’être promus agents spéciaux de la cybersécurité ! Ils entrent dans la salle des réseaux pour une visioconférence avec la Directrice générale quand soudain...

« Vous m’avez renvoyé de l’académie il y a quelques mois… Vous allez payer. Une bombe virtuelle surpuissante a été placée sur le réseau informatique de l’académie et explosera dans 45 minutes, vous ne pouvez qu’attendre dans l’angoisse et contempler les dégâts qu’elle va causer. Hasta la vista. »

Les joueurs ont 45 minutes pour désamorcer la bombe informatique. À eux de fouiller la salle des réseaux vide de ses occupants, partis en séminaire, et rechercher des indices laissés par un éventuel complice au sein de l’entreprise.

## La vidéo d'introduction : 
https://podeduc.apps.education.fr/video/10857-cyberescape/

## Ressources pour mettre en place le jeu
Voir les fichiers déposés
Un diaporama de synthèse peut servir de support de débriefing avec les élèves

## Articles décrivant le jeu
https://scape.enepe.fr/cyberescape.html
https://www.dane.ac-versailles.fr/spip.php?article525

## Auteure
Mélanie Fenaert (professeure de SVT et SNT, ac Versailles)
melanie.fenaert@ac-versailles.fr

## Licence
CC BY NC SA





